package csvquoteparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;

import org.junit.Test;

public class CSVQuoteParserTest {

	private static final class MockLineReader implements LineReader {
		String line;

		MockLineReader(String line) {
			this.line = line;
		}

		public String nextLine() {
			return line;
		}
	}


	LineReader emptyTextSource = new MockLineReader(null);

	@Test
	public void shouldReturnNullOnEmptySource() throws Exception {
		CSVQuoteSource quoteSource = new CSVQuoteSource(emptyTextSource);
		assertNull(quoteSource.next());
	}

	@Test
	public void parseQuoteLine() throws Exception {
		CSVQuoteSource quoteSource = new CSVQuoteSource(//
				makeSource("ATT,10.0,10.2,1970-01-01,00:00:00"));
		
		Quote q = quoteSource.next();
		assertEquals("ATT", q.symbol());
		assertEquals(10d, q.bid(), 0);
		assertEquals(10.2d, q.ask(), 0);
		assertEquals(0L, q.time());
	}

	@Test
	public void parseQuoteLine1() throws Exception {
		LocalDateTime dateTime = LocalDateTime.of(2019, Month.JUNE, 4, 10, 0, 2);
		
		MockLineReader mockLineReader = makeSource("ATT,10.0,10.2,"
				+ dateTime.toLocalDate().toString()
				+ ","
				+ dateTime.toLocalTime().toString()
				+ "");
		
		CSVQuoteSource quoteSource = new CSVQuoteSource(//
				mockLineReader);

		Quote q = quoteSource.next();
		assertEquals("ATT", q.symbol());
		assertEquals(10d, q.bid(), 0);
		assertEquals(10.2d, q.ask(), 0);
		long quoteTime = dateTime.toInstant(ZoneOffset.UTC).toEpochMilli();
		assertEquals(quoteTime, q.time());
	}

	private MockLineReader makeSource(String line) {
		return new MockLineReader(line);
	}
}