package csvquoteparser;

import static java.lang.Double.parseDouble;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class CSVQuoteSource implements QuoteSource {

	private LineReader lineReader;

	public CSVQuoteSource(LineReader lineReader) {
		this.lineReader = lineReader;
	}

	public Quote next() {
		String line = lineReader.nextLine();
		if (line == null)
			return null;

		return parseCsvLine(line);
	}

	private Quote parseCsvLine(String line) {
		String tokens[] = line.split("[,]");
		String symbol = tokens[0];
		double bid = parseDouble(tokens[1]);
		double ask = parseDouble(tokens[2]);
		long time = parseDateTime(tokens[3], tokens[4]);
		return new Quote(symbol, bid, ask, time);
	}

	private long parseDateTime(String dateStr, String timeStr) {
		LocalDateTime ldt = LocalDateTime.parse(dateStr + "T" + timeStr);
		return ZonedDateTime.of(ldt, ZoneOffset.UTC).toInstant().toEpochMilli();
	}

}