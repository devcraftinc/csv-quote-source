package csvquoteparser;

public interface QuoteSource {

	Quote next();

}

class AlgoTradingEngine {

	private QuoteSource quoteSource;

	public AlgoTradingEngine(QuoteSource quoteSource) {
		this.quoteSource = quoteSource;
	}
	
	
	public void trade() {
		Quote q = quoteSource.next();
		while (q != null) {
			makeALotOfMoney(q);
			q = quoteSource.next();
		}
	}


	private void makeALotOfMoney(Quote q) {
		// TODO Auto-generated method stub
		
	}
}