package csvquoteparser;

public interface LineReader {
	public String nextLine();
}