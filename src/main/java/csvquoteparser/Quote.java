package csvquoteparser;

public class Quote {

	private String symbol;
	private double bid;
	private double ask;
	private long time;
	
	public Quote(String symbol, double bid, double ask, long time) {
		this.symbol = symbol;
		this.bid = bid;
		this.ask = ask; 
		this.time = time;
	}


	public String symbol() {
		return symbol;
	}


	public double bid() {
		return bid;
	}


	public double ask() {
		return this.ask;
	}


	public long time() {
		return this.time;
	}

}